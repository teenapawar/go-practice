package f1

import (
	"testing"

	"github.com/go-playground/assert/v2"
)

func Test_fun1(t *testing.T) {
	y := 2
	fun1(&y)
	expected := 4
	assert.Equal(t, y, expected)
}
