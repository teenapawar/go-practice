package main

import "fmt"

// func ConvertToBytes(data interface{}) {
// 	d := []byte{}
// 	switch data.(type) {
// 	case []byte:
// 		d,_ = data.(string)
// 	case string:
// 		d = []byte(t)
// 	}

// }

func transformVariableToConcreateType(data interface{}) {
	bytes := []byte{}
	fmt.Println(data)
	switch d := data.(type) {
	case []byte:
		fmt.Println("-----------", d)
		bytes = d //not able to assign data instead of d becoz after switch data is only accessible by data.(type) and also
					// cant use bytes=data.(type)becoz data.(type)is only used with switch so assign d := data.(type)
	case string:
		bytes = []byte(d)
		fmt.Println(bytes)
		fmt.Println("++++++++++", d)
	
	// case int:
	// 	bytes=[]byte{d}	//cant convert int to []byte
	}
	processData(bytes)
}
func processData(data []byte) {
	fmt.Println("Processing data: ", data)
}

func main() {
	var a1 byte = 97
	transformVariableToConcreateType(a1)
	transformVariableToConcreateType([]byte{61,62})
}
