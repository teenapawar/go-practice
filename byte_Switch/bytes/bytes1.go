package main

import (
	"bytes"
	"fmt"
)


func main(){
	var buf bytes.Buffer
	buf.WriteByte(32)
	buf.Write([]byte{67,68})
	buf.WriteByte(32)
	buf.WriteByte(69)
	buf.WriteByte(32)
	buf.WriteString("abc")
	fmt.Println(buf)
	buf.WriteByte(32)


	fmt.Println(buf.String())


	fmt.Println("+++++++++++++++",buf.Bytes())
	fmt.Println("***************",buf)


	remove_1st_last:=bytes.Trim(buf.Bytes()," ")
	fmt.Println(remove_1st_last)
	fmt.Println(string(remove_1st_last))



	buf_withField:=bytes.Fields(buf.Bytes())
	for _, e := range buf_withField {
		fmt.Printf("%s ", string(e))
   }

   buf2:= []byte{69,97}
   buf3:= []byte{67,68}
   result:=bytes.Contains(buf.Bytes(),buf2)	//fasle becoz 69,97 to far
   result2:=bytes.Contains(buf.Bytes(),buf3)
   fmt.Println(result,result2)
	
}