package main

import (
	"fmt"
	"time"
)

func main() {
	var i = 2
	switch i {
	case 1:
		println("one")
	case 2:
		println("two")
	default:
		println("no")
	}

	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		println("weekends")
	default:
		println("weekday")

	}

	switch {
	case time.Now().Hour() < 12:
		println("morning")
	case time.Now().Hour() < 18:
		println("evening")

	}
	var j int = 2
	switch {
	case j < 1:
		println("false")
	case j == 2:
		println("true")
	}

	//string type is only for interface

	whatAmI := func(i interface{}) {
		switch i.(type) {
		case bool:
			println("i am bool")
		case int:
			fmt.Println("I'm an int")
		default:
			fmt.Println("Don't know type")
		}
	}
	whatAmI(true)
	whatAmI(1)
	whatAmI("hey")

	name("string")
	name(true)
}

func name(i interface{}) {
	switch t := i.(type) {
	case bool:
		println("===i am bool",t)
	case int:
		fmt.Println("==I'm an int", t)
	default:
		fmt.Printf("==%vDon't know type", t)
	}

	//interface------

	var x interface{} = "foo"
	fmt.Println("============", x)
	fmt.Printf("%T", x)

	var s string = x.(string)
	fmt.Println(s) // "foo"
	fmt.Printf("%T", s)

	s, ok := x.(string)
	fmt.Println(s, ok) // "foo true"
	fmt.Printf("-----------%T\n", x)

	n, ok := x.(int)
	fmt.Println(n, ok) // "0 false"

	//n = x.(int) // ILLEGAL


	switch day := 4;day {
	case 1:
		fmt.Println("Monday")
	case 2:
		fmt.Println("Tuesday")
	case 3:
		fmt.Println("Wednesday")
	case 4:
		fmt.Println("Thursday")
	case 5:
		fmt.Println("Friday")
	case 6:
		fmt.Println("Saturday")
	case 7:
		fmt.Println("Sunday")
	default:
		fmt.Println("Invalid")

	}

}
