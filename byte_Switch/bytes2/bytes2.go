package main

import (
	"bytes"
	"fmt"
	"os"
)

func main() {
	var strBuffer bytes.Buffer
	fmt.Fprintf(&strBuffer, "It is number: %d, This is a string: %v\n", 10, "Bridge")
	strBuffer.WriteString("[DONE]")
	fmt.Println("The string buffer output is", strBuffer.String())

	fmt.Println("-----------------------------")

	var byteString bytes.Buffer
	byteString.Write([]byte("Hello "))
	fmt.Fprintf(&byteString, "Hello friends how are you")
	fmt.Println("++++++++++++++",byteString.String())
	byteString.WriteTo(os.Stdout)
	fmt.Println("\n-----------------------------")

	var strByyte = []byte("Ranjan, Kumar")
	//strByyte = bytes.TrimPrefix(strByyte, []byte("Hello, "))
	//strByyte = bytes.TrimPrefix(strByyte, []byte("Good we will meet again,"))
	fmt.Printf("Hello%s", strByyte)
}
