package main

import "fmt"

func main() {
	functionEvaluation()
}

func functionEvaluation() {
	num := 12
	switch b := isEven(num);{
	case false:
		fmt.Printf("Num is odd %v %t\n", num, b)
	case true && num < 5:
		fmt.Printf("Num is even %v %t\n", num, b)
	case true && num > 10:
		fmt.Printf("Num is even %v, and greater than 10\n", num)
	}
}
func isEven(n int) bool {
	if n%2 == 0 {
		return true
	}
	return false
}
