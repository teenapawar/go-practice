package main

import "fmt"

func updateModel(model interface{}) {
	switch t := model.(type) {
	case *User:
		updateUser(t)
	case User:
		updateUser(&t)
	case *Post:
		updatePost(t)
	case Post:
		updatePost(&t)
	case *Chat:
		updateChat(t)
	case Chat:
		updateChat(&t)
	}
}
func updateUser(user *User) {
	fmt.Printf("Updating user %v\n", user.Name)
}
func updatePost(post *Post) {
	fmt.Printf("Updating post %v\n", post.Title)
}
func updateChat(chat *Chat) {
	fmt.Printf("Updating chat %v\n", chat.Participants)
}

type User struct{
	Name  string
}

type Post struct{
	Title  string
}

type Chat struct{
	Participants  string
}

func main(){

	updateModel(User{Name: "John"})
	updateModel(&User{Name: "Mary"})
	updateModel(Post{Title: "Post 1"})
	updateModel(&Post{Title: "Post 2"})
	updateModel(&Chat{})
}