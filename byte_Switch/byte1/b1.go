package main

import (
     "bytes"
     "fmt"
)

func main() {

     data1 := []byte{102, 97, 108, 99, 111, 110} // falcon
     data2 := []byte{111, 110,106}                   // on

     if bytes.Contains(data1, data2) {
          fmt.Println("contains")
     } else {
          fmt.Println("does not contain")
     }

     if bytes.Equal([]byte("falcon"), []byte("owl")) {
          fmt.Println("equal")
     } else {
          fmt.Println("not equal")
     }

     data3 := []byte{111, 119, 108, 9, 99, 97, 116, 32, 32, 32, 32, 100, 111,
          103, 32, 112, 105, 103, 32, 32, 32, 32, 98, 101, 97, 114}
	fmt.Println("------------",string(data3))
     fields := bytes.Fields(data3)
     fmt.Println(fields)

     for _, e := range fields {
          fmt.Printf("%s ", string(e))
     }

     fmt.Println()
     b:=[][]byte{[]byte("ab fd"),[]byte("cd"),[]byte{1,2}}     //not used[]byte{1,2}
     new_b :=bytes.Join(b,[]byte("ghj"))
     fmt.Println(new_b)
     fmt.Println(string(new_b))

     fmt.Println("----------------------------------------")

     dataa2 := []byte{102, 97, 108, 99, 111, 110, 32}
     dataa3 := bytes.Repeat(dataa2, 3)

     fmt.Println(dataa3)
     fmt.Println(string(dataa3))

     fmt.Println("--------------------------")

     dataa4 := []byte{108,32, 32, 102, 97, 108, 99, 111, 110, 32, 32, 32,108}
     dataa5 := bytes.Trim(dataa4, "l")//bytes.Trim(dataa4, " ") removing 1st and last element 

     fmt.Println(dataa5)
     fmt.Println(string(dataa5))
    c:= bytes.Count(dataa4,dataa2)
    d:= bytes.Count(dataa4,data2)
    fmt.Println(c,d)
   
     
}

