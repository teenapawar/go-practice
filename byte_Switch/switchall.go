package main

import (
	"fmt"
)

type User struct {
	Id, Name, LastName string
}

type Post struct {
	Title, Description string
}

type Chat struct {
	Participants []*User
}

func main() {
	variableEvaluation()
	statusEvaluation()
	expressionEvaluation()
	functionEvaluation()
	typeSwitch([]int{1, 2, 3, 4, 5})
	typeSwitch([]string{"name", "lastName"})
	transformVariableToConcreateType("some data")
	transformVariableToConcreateType([]byte("more data"))
	updateModel(User{Name: "John"})
	updateModel(&User{Name: "Mary"})
	updateModel(Post{Title: "Post 1"})
	updateModel(&Post{Title: "Post 2"})
	updateModel(&Chat{})
}

func updateModel(model interface{}) {
	switch t := model.(type) {
	case *User:
		updateUser(t)
	case User:
		updateUser(&t)
	case *Post:
		updatePost(t)
	case Post:
		updatePost(&t)
	case *Chat:
		updateChat(t)
	case Chat:
		updateChat(&t)
	}
}

func updateUser(user *User) {
	fmt.Printf("Updating user %v\n", user.Name)
}

func updatePost(post *Post) {
	fmt.Printf("Updating post %v\n", post.Title)
}

func updateChat(chat *Chat) {
	fmt.Printf("Updating chat %v\n", chat.Participants)
}

func transformVariableToConcreateType(data interface{}) {
	bytes := []byte{}
	switch d := data.(type) {
	case []byte:
		bytes = d
	case string:
		bytes = []byte(d)
	}

	processData(bytes)
}

func processData(data []byte) {
	fmt.Println("Processing data: ", data)
}

func typeSwitch(elements interface{}) {
	switch t := elements.(type) {
	case int:
		fmt.Printf("Type integer %T\n", t)
	case bool:
		fmt.Printf("Type boolean %T\n", t)
	case []int:
		fmt.Printf("Type slice of integers %T\n", t)
	case []string:
		fmt.Printf("Type slice of strings %T\n", t)
	default:
		fmt.Println("Unkown type")
	}
}

func functionEvaluation() {
	num := 12
	switch b := isEven(num); {
	case false:
		fmt.Printf("Num is odd %v %t\n", num, b)
	case true && num < 5:
		fmt.Printf("Num is even %v %t\n", num, b)
	case true && num > 10:
		fmt.Printf("Num is even %v, and greater than 10\n", num)
	}
}

func isEven(n int) bool {
	if n%2 == 0 {
		return true
	}

	return false
}

func expressionEvaluation() {
	num := 10
	switch {
	case num > 10 && num < 20:
		num /= 2
	case num >= 20:
		num *= 2
	case num <= 10 && num > 0:
		num += 100
	default:
		fmt.Println("Invalid number")
	}
	fmt.Println("Num: ", num)
}

func statusEvaluation() {
	status := "active"
	switch status {
	case "active", "inactive":
		fmt.Println("Update user")
	case "deleted":
		fmt.Println("Hard Delete user")
	}
}

func variableEvaluation() {
	name := "John"
	switch name {
	case "John":
		fmt.Println("Hi it is John")
	case "Carl":
		fmt.Println("Hi it is Carl")
	case "Paul":
		fmt.Println("Hi it is Paul")
	default:
		fmt.Println("Unkown name")
	}
}