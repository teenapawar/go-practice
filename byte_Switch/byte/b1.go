package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	v := []byte("teena")
	fmt.Println(v)
	s := []byte(v)
	fmt.Println(s)
	z := []byte{67, 69}
	fmt.Print(z)
	// zx :=[]byte(69)
	// fmt.Print(zx)	//cant convert []byte(69)to byte , if []byte{69}is there so it is possible
	var a1 byte = 97 //declaration
	fmt.Println(a1)

	data := []byte{69, 68, 67}
	fmt.Println(data)
	fmt.Println([]byte(data))
	fmt.Println(string(data))
	//fmt.Println(data(string))

	content, err := ioutil.ReadFile("words.txt")
	fmt.Println("----",string(content),"+++++=",err)

}
