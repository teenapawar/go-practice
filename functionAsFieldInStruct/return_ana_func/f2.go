package main
import "fmt"

func greet() func() {

  return func() {
    fmt.Println("Hi John")
  }

}

func main() {

  g1 := greet()
  g1()
}