package main

import "fmt"

func f2() {
	fmt.Println("func f2...")
}

type f1 struct {
	functionInGo func()
}

func main() {
	o := f1{functionInGo: f2}
	o.functionInGo()
	//o.f2=func(){dunctionInGo:fmt.Println("No")}

}
