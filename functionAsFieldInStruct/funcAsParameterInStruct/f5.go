package main

import (
	"fmt"
)

type Foo struct {
	Bar func()
}

func main() {
	f := Foo{
		Bar: func() { fmt.Println("initial") },
	}
	f.Bar()

	f.Bar = func() { fmt.Println("changed") }
	f.Bar()
}