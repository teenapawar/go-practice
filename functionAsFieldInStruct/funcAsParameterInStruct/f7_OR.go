package main

import "fmt"

func Info(n string, o string, a int) string {

	return fmt.Sprintf("%s is %d years old and he is a %s\n", n, a, o)
}

type User struct {
	name       string
	occupation string
	age        int
	info       func(string, string, int) string
}

func main() {

	u := User{name: "teena",occupation: "developer",age: 23,info: Info}
	fmt.Println(u.info(u.name,u.occupation,u.age))

}
