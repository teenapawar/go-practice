package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"time"
)

type Plant struct {
	XMLName xml.Name `xml:"plant"`
	Id      int      `xml:"id,attr"`
	Name    string   `xml:"name"`
	Origin  []string `xml:"origin"`
}

// func (p Plant) String() string {
//     return fmt.Sprintf("Plant id=%v, name=%v, origin=%v",
//         p.Id, p.Name, p.Origin)
// }

func main() {
	coffee := &Plant{Id: 27, Name: "Coffee"}
	coffee.Origin = []string{"Ethiopia", "Brazil"}

	out, _ := xml.MarshalIndent(coffee, " ", "  ")
	fmt.Println("--------------\n", out, "\n------------------")
	fmt.Println(string(out))

	fmt.Println("*************\n", xml.Header+string(out), "\n********************")

	var p Plant
	if err := xml.Unmarshal(out, &p); err != nil {
		panic(err)
	}
	fmt.Println(p)

	fmt.Println("000000000000000000000000000")
	err := ioutil.WriteFile("teena.txt", out, 0666)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("000000000000000000000000000")
	fmt.Println("$$$$$$$$$$$$$$$$$$$$$$$$$$")
	data, err := ioutil.ReadFile("teena.txt")
	fmt.Println("@@@@@@@@@@@@@", data)
	coffee = &Plant{}
	err = xml.Unmarshal([]byte(data), &coffee)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(coffee.Name)
	fmt.Println("000000000000000000000000000")

	t := time.Now()
	fmt.Println(t.Format("1:03PM"))

}
