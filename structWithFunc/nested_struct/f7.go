package main

import "fmt"

type Address struct {
	city    string
	country string
}

type User struct {
	name string
	age  int
	Address
}

func main() {

	p := User{							//	p:=User{
		"John Doe",						//			name: "John Doe",
		34,								//			age:  34,
		Address{						//			 Address{			//---error [mixture of field:value and value initializers]
																		// becoz u are initialize name,age but not initialize address:adress
																		//so it is not allowed
			city:    "New York",		//					city:    "New York",
			country: "USA",				//					country: "USA",
		},								//				}
	}									//		}

	// p := User{
    //     name: "John Doe",
    //     age:  34,
    //     Address: Address{			//-----this one also ok see above comment that is not ok
    //         city:    "New York",
    //         country: "USA",
    //     },
    // }

	fmt.Println("Name:", p.name)
	fmt.Println("Age:", p.age)
	fmt.Println("City:", p.city)
	fmt.Println("Country:", p.country)

}

//-----------------or-----------------------
// package main

// import "fmt"

// type Address struct {
//     city    string
//     country string
// }

// type User struct {
//     name    string
//     age     int
//     address Address
// }

// func main() {

//     p := User{
//         name: "John Doe",
//         age:  34,
//         address: Address{
//             city:    "New York",
//             country: "USA",
//         },
//     }

//     fmt.Println("Name:", p.name)
//     fmt.Println("Age:", p.age)
//     fmt.Println("City:", p.address.city)
//     fmt.Println("Country:", p.address.country)
// }
