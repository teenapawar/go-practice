package main

import (
	"fmt"
	"time"
)

type myTime struct {
	time.Time //anonymous field
}

func (t myTime) first5Chars() string {
	return t.Time.String()[0:35]	//here t(myTime)struct directly call the method of Time struct i.e time.string
									// dont need to write t.Time.time.string()
}

func main() {
	m := myTime{time.Now()}                 //since time.LocalTime returns an address, we convert it to a value with *
	fmt.Println("Full time now:", m.String())      //calling existing String method on anonymous Time field
	fmt.Println("First 5 chars:", m.first5Chars()) //calling myTime.first5Chars
}
