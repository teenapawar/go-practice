package main

import "fmt"


type Rect struct {
	len, wid int
}

func (r Rect) area_by_value() int {
	r.len=1
	return r.len * r.wid
}

func (r *Rect)area_by_reference()int{
	//r.len=1		//1. uncommnent this line 2.check area_by_value by commenting there r.len
	return r.len * r.wid
}
func main(){
	r :=Rect{10,12}
	fmt.Println(r)
	fmt.Println("Area of Rectangle is:", r.area_by_value())
    fmt.Println("Area of Rectangle is:", r.area_by_reference())
    fmt.Println("Area of Rectangle is:", (&r).area_by_value())
    fmt.Println("Area of Rectangle is:", (&r).area_by_reference())
	


}