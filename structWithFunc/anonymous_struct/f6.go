package main

import "fmt"

// func main() {
// 	u := struct {
// 		name string
// 		age  int
// 	}{
// 		name: "teena",
// 		age:  23,
// 	}
// 	fmt.Println(u.age)
// }

type s struct {
	age       int
	anaStruct struct{}
}

func main() {
	sobj:=s{age:23,anaStruct: struct{}{}}
	fmt.Println(sobj.anaStruct)
	fmt.Println(sobj.age)
}
