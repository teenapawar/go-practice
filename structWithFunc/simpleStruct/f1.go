package main

import "fmt"

type Rect struct {
	len, wid int
}

func (r Rect) area() int {
	return r.len * r.wid
}

func main() {
	r := Rect{2, 3}
	fmt.Println(r.area())
}
