package main

import "fmt"

type User struct {
    name       string
    occupation string
    age        int
}

func main() {

	//New does not initialize the memory, it only zeros it. It returns a pointer to a newly allocated zero value.
	// Make creates slices, maps, and channels only, and it returns them initialized.

    u := new(User)	// we can create a pointer to a struct with the new keyword.
	fmt.Println(u)
    u.name = "Richard Roe"
    u.occupation = "driver"
    u.age = 44

    fmt.Printf("%s is %d years old and he is a %s\n", u.name, u.age, u.occupation)
}