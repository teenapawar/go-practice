package main

import (
	"flag"
	"fmt"
)

func main() {
	s := flag.Int("s", 0, "start line no")
	t := flag.Int("t", 0, "end line no")

	flag.Parse()

	fmt.Printf("Search file from line number %d to %d\n", *s, *t)

	fmt.Println("for keywords:", flag.Args())

	i := 0
	fmt.Printf("The keyword at index %d: %v\n", i, flag.Arg(i))

}
