//go build f1.go 
//./f1 -name=teena -age=12 -country="UK"
//./f1 -name=teena -country=UKING

package main

import (
	"flag"
	"fmt"
)

func main(){
	name := flag.String("name","guest","specify your name")
	age := flag.Int("age",0,"specify your age")
	var country string
	flag.StringVar(&country,"country","india","enter your country")
	flag.Parse()
	fmt.Printf("Hello %s\n", *name)
	fmt.Printf("Your age is %d\n", *age)
	fmt.Printf("Your are from %s\n", country)
}