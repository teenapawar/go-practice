package main

import (
	"fmt"
	"math"
)
type newInter interface {
	area() float64
}

type rec struct {
	len, wid float64
}

func (r rec) area() float64 {
	return r.len * r.wid
}

type Circle struct {
	Radius float64
}

func (c Circle) area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// Generic function to calculate the total area of multiple shapes of different types
func CalculateTotalArea(shapes ...newInter) float64 {
	totalArea := 0.0
	for _, s := range shapes {
		totalArea += s.area()
	}
	return totalArea
}

func main() {
	totalArea := CalculateTotalArea(Circle{2}, rec{4, 5}, Circle{10})
	fmt.Println("Total area = ", totalArea)
}
