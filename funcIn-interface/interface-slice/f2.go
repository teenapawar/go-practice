package main

import (
	"fmt"
	"math"

	
)

type Shape interface {
	area() float64
}

type rec struct {
	len, wid float64
}

func (r rec) area() float64 {
	return r.len * r.wid
}

type Circle struct {
	Radius float64
	//numberst float64
}

func (c Circle) area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// Interface types can also be used as fields
type MyDrawing struct {
	shapes  []Shape
	bgColor string
	fgColor string
}

func (drawing MyDrawing) Area() float64 {
	totalArea := 0.0
	for _, s := range drawing.shapes {
		fmt.Println("----------------",s)
		fmt.Println("++++++++++++",totalArea)
		totalArea += s.area()
	}
	return totalArea
}

func main() {
	drawing := MyDrawing{
		shapes: []Shape{
			Circle{2},
			rec{3, 5},
			rec{4, 7},
		},
		bgColor: "red",
		fgColor: "white",
	}
	fmt.Println(drawing.shapes)

	fmt.Println("Drawing", drawing)
	fmt.Println("Drawing Area = ", drawing.Area())
}
