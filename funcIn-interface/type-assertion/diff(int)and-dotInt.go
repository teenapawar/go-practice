package main

import (
	"fmt"
)

func main() {

	user := make(map[string]float64, 0)

	// user["name"] = "John Doe"
	user["age"] = 21.9
	// user["weight"] = 70.3

	ok := int(user["age"])

	//ok += 3

	fmt.Println(ok)
	user["age"] = float64(ok + 1)

	fmt.Printf("%+v", user)
}
