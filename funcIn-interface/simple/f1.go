package main

import (
	"fmt"
	"math"
)

type newInter interface {
	area() float64
}

type rec struct {
	len, wid float64
}

func (r rec) area() float64 {
	return r.len * r.wid
}

type Circle struct {
	Radius float64
}

func (c Circle) area() float64 {
	return math.Pi * c.Radius * c.Radius
}

func checkInter(n newInter) {
	fmt.Println(n.area())
}

func main() {
	r := rec{2,3}
	c := Circle{3}
	checkInter(r)
	checkInter(c)

}
