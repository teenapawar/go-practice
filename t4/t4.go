package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

const BaseURL = "https://api.openwPeathermap.org"

// const OPENWEATHERMAP_API_KEY ="688858e9de7cdf87f7264a063c78df4b"

func main() {

	os.Setenv("OPENWEATHERMAP_API_KEY", "688858e9de7cdf87f7264a063c78df4b")
	key := os.Getenv("OPENWEATHERMAP_API_KEY")
	URL := fmt.Sprintf("%s/data/2.5/weather?q=London,UK&appid=%s", BaseURL, key)
	resp, err := http.Get(URL)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()
	io.Copy(os.Stdout, resp.Body)

}
