package pointerplay

import (
	"errors"
	"fmt"
)

type MyFloat float64

func (x *MyFloat) Double() {
	(*x) *= 2.0
	fmt.Printf("x: %+v\n", *x)

	// return x

}

func Add(value1, value2 float64) (float64, error) {
	if value1 > 0 && value2 > 0 {
		return value1 + value2, nil
	}
	return 0, errors.New("value should not be zero or greater then zero")
}
