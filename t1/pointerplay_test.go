package pointerplay

import (
	
	"testing"

	"github.com/go-playground/assert/v2"
)

// func Test_Double(t *testing.T) {
// 	x := 2
// 	expected := 4
// 	Double(x)
// 	// assert.Equal(t,expected)
// 	if expected != x {
// 		t.Errorf("want %d, got %d", expected, x)
// 	}
// }

func Test_Double(t *testing.T) {

	y := MyFloat(2.5)
	(&y).Double()

	expected := float64(5.0)
	assert.Equal(t, float64(y), expected)
}
func Test_Add(t *testing.T) {

	t.Run("when 2 values are greater than zero", func(t *testing.T) {
		actual, err := Add(10.5, 10.5)
		expected := float64(21)
		assert.Equal(t, actual, expected)
		assert.Equal(t, err, nil)
	})
	t.Run("when any args are zero and less than zero", func(t *testing.T) {
		actual, err := Add(0, 10.5)
		assert.Equal(t, err != nil, true)
		assert.NotEqual(t, actual, float64(10.5))
	})

}
