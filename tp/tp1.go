package main

import (
    "fmt"
    "net"
    "net/url"
)

func main() {

    s := "localhost:7001/trans@actions?prefix=<transactionKey>"
	 

    u, err := url.Parse(s)
    if err != nil {
        panic(err)
    }

    fmt.Println(u.Scheme)

    fmt.Println("****************8",u.User)
    fmt.Println("----------------",u.User.Username())
    p, _ := u.User.Password()
    fmt.Println("****************8",p)

    fmt.Println("----------------",u.Host)
    host, port, _ := net.SplitHostPort(u.Host)
    fmt.Println("****************8",host)
    fmt.Println("----------------",port)

    fmt.Println("****************8",u.Path)
    fmt.Println("----------------",u.Fragment)

    fmt.Println("****************8",u.RawQuery)
    m, _ := url.ParseQuery(u.RawQuery)
    fmt.Println("----------------",m)
    fmt.Println("----------------",m["prefix"][0])
}